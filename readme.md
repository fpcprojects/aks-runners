# AKS Runners

These are the scripts to setup the runners for the fpcprojects-gitlab group. The runners are running on an Azure Kubernetes Service (AKS) provided by CNOC.

There are Windows and Linux runners. The Windows runners are in a on-demand (scaled) nodepool, and can take a long time to load. The Windows nodepools are also [spot](https://docs.microsoft.com/en-us/azure/aks/spot-node-pool) nodepool, which means that a runner can be aborted at any time. Time will tell if this is annoying or not. (It is cheap, though, and CNOC does not use Windows nodes for anything else, so the workload can not be shared. But as long as Freepascal suports legacy systems like Windows, this could be a good solution.)

## Getting started

### Prerequisites

You need an [AKS cluster](https://azure.microsoft.com/nl-nl/services/kubernetes-service/) but it should work with other Kubernetes clusters. And you need to have the [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html) repository added to your Helm-configuration.

### Installing

At this moment (aug 2021) the Windows runners only work with the Gitlab Runners Helm chart versions 0.32.0 and 0.29.0, but not with the versions 0.30.0 and 0.31.0.

Adapt the `values_runner_fpcprojects_windows.yaml` and `values_runner_fpcprojects_linux.yaml` files so that they contain the runnerRegistrationToken you can get from the CI/CD-Runners configuration section in Gitlab.

These commands are used to start the Kubernetes agents:

    helm3 install --namespace gitlab gitlab-runner-fpcprojects-windows -f values_runner_fpcprojects_windows.yaml gitlab/gitlab-runner --kubeconfig ~/.kube/config

    helm3 install --namespace gitlab gitlab-runner-fpcprojects-linux -f values_runner_fpcprojects_linux.yaml gitlab/gitlab-runner --kubeconfig ~/.kube/config

To upgrade the configuration, replace `install` with `upgrade`.

### Basic usage

The runners come with the 'linux' and 'windows' tags, which can be used to distinguish between builds. This is an example for a .gitlab-ci.yml file:

    stages:
    - build

    windows build:
      image: fppkg.azurecr.io/fpc-322-windows-buildimage
      stage: build
      tags:
      - windows
      script:
          - fppkg build
          - fppkg archive
      artifacts:
          paths:
          - fpcdab-*.source.zip
          expire_in: 1 month

    linux build:
      image: quay.io/loesje/fpc-322-linux-buildimage
      stage: build
      tags:
      - linux
      script:
          - fppkg build
          - fppkg archive
      artifacts:
          paths:
          - fpcdab-*.source.zip
          expire_in: 1 month

## General remarks about getting all this to work on Windows

The support for Kuberneter-runners on Windows nodes is brand new. I had to make the following adaptions to values_runner_fpcprojects_windows.yaml. (See this file for the exact syntax):

 - Set `"kubernetes.io/os" = "windows"`  As I [understood](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4014) this should trigger some mechanism to set the other properties right for Windows nodes, but thid did not work for me. So I had to make the following changes:
   - Set `shell = "pwsh"`
 - Increased `poll_timeout`, as some Windows systems are slow starters (especially ona on-demand nodepool) and downloading the huge containers also take some time.
 - Set `"node.kubernetes.io/windows-build" = "10.0.17763"`. The correct Windows-build version can be found with `kubectl describe <windows-node-name>`.
 - set `FF_USE_POWERSHELL_PATH_RESOLVER = true`
 - Note that the build-images should be based on the Powershell-containers from Microsoft. When I tried a clean `servercore` image, it failed.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

## License

Do with it whatever you want, these are more examples than actual code or copyrighted work.