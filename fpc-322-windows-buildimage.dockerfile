FROM mcr.microsoft.com/powershell:lts-windowsservercore-1809

ADD http://amira.cnoc.nl/fpc/fpc-3.2.2.win32.and.win64.exe /temp/fpc-3.2.2.win32.and.win64.exe
ADD https://curl.se/windows/dl-7.78.0/openssl-1.1.1k-win32-mingw.zip /temp/openssl-1.1.1k-win32-mingw.zip

ADD https://github.com/git-for-windows/git/releases/download/v2.33.0.windows.1/MinGit-2.33.0-64-bit.zip /temp/mingit.zip

WORKDIR c:/temp/

RUN /temp/fpc-3.2.2.win32.and.win64.exe /verysilent /norestart /log=c:\\temp\\fpcinstall.log
RUN unzip.exe c:\\temp\\mingit.zip -d c:\\MinGit
RUN unzip.exe -j c:\\temp\\openssl-1.1.1k-win32-mingw.zip openssl-1.1.1k-win32-mingw/*.dll openssl-1.1.1k-win32-mingw/LICENSE.txt -d C:\\FPC\\3.2.2\\bin\\i386-win32\\
